# -*- coding: utf-8 -*-

# USING PLOTLY VERSION 4.8!!!!!!!!!!!!!!!!

import locale

locale.setlocale(locale.LC_ALL, '')

import pandas as pd
# import geopandas as gpd
import sqlite3
import plotly.graph_objs as go
import re
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from scipy import stats
# import pysal as ps
from colormap import rgb2hex
from shapely import wkt
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.colors import n_colors
from matplotlib import cm
import locale
import datetime as dt


########################################################################################################################


def getHex(c):
    '''

    :param c: RGB color in string normalized (Matplotlib standard)
    :return: Hexcolor in string
    '''
    hexColor = re.findall('[0-9]{1,3}.[0-9]*', c)
    hexColor = rgb2hex(int(float(hexColor[0])), int(float(hexColor[1])), int(float(hexColor[2])))

    return hexColor


def makeColorMap(mplCMap='magma_r', steps=7):
    cmap = cm.get_cmap(mplCMap)
    stepList = np.arange(0.0, 1.0, 1.0 / (steps - 1)).tolist() + [1.0]

    colorList = []

    for i in stepList:
        hexcolor = cm.colors.to_hex(cmap(i))
        colorList.append(hexcolor)

    return colorList


def makeColorScale(c1, c2, n):
    '''

    :param c1: first colors of the scale
    :param c2: last color of the scale
    :param n: number of steps
    :return: list of colors in rgb
    '''
    colors = n_colors(c1, c2, n, colortype='rgb')

    colors = [getHex(c) for c in colors]

    return colors


def makeDataFrame(db, query):
    '''

    :param db: path to the sqlite file
    :param query: table query
    :return: dataframe
    '''
    CON_IN = sqlite3.connect(db)
    df = pd.read_sql(sql=query, con=CON_IN)

    return df


# def makeGeoDataFrame(db, query, srid):
#     '''

#     :param db: path to the sqlite file
#     :param query: table query, MUST cast geometry as text and call ir geom
#     :param srid: projection as INTEGER
#     :return: dataframe
#     '''
#     CON_IN = sqlite3.connect(db)
#     CON_IN.enable_load_extension(True)
#     CON_IN.execute('SELECT load_extension("mod_spatialite.so")')

#     dfTemp = pd.read_sql(sql = query, con = CON_IN)
#     dfTemp['geom'] = dfTemp['geom'].apply(wkt.loads)
#     gdf = gpd.GeoDataFrame(dfTemp, geometry = 'geom', crs = srid)

#     return gdf


def makeHisto(dataFrame, col, title='title', y_title='y', x_title='x', n_bins=10, orientation='v', barmode='group',
              barspace=0.2, size=(500, 500), font='Arial', fontfactor=1, colors=None, legend=True, export=False,
              file_name=''):
    '''

    :param dataFrame: dataframe
    :param col: columns with the values, list for multiple column names or string for a single one
    :param title: title of the chart, DEFAULT = title
    :param y_title: title of the Y axis, DEFAULT = y
    :param x_title: title of the X axis, DEFAULT = x
    :param n_bins: number of bins to aggregate, DEFAULT = 10
    :param orientation: string h for horizontal and v for vertical, DEFAULT = v
    :param barmode: string group for grouped bars and stack for stacked, DEFAULT = group
    :param barspace: percentage (0 to 1) of the width of the bars to be applied as space between group bars, DEFAULT = 0.2
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (500, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''
    data = []
    w = size[0]
    h = size[1]

    if orientation == 'v':
        if type(col) is str:
            x = col
            color = 'rgb(0, 0, 0)'

            trace = go.Histogram(
                x=dataFrame[x],
                nbinsx=n_bins,
                marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                name=x[:20],
                orientation=orientation,
                histfunc='count',
                histnorm='percent'
            )

            data.append(trace)

        elif type(col) is list:
            for i in list(range(len(col))):
                x = col[i]
                color = colors[i] if colors is not None else 'rgb(0, 0, 0)'

                trace = go.Histogram(
                    x=dataFrame[x],
                    nbinsx=n_bins,
                    marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                    name=x[:20],
                    orientation=orientation,
                    histfunc='count',
                    histnorm='percent'
                )

                data.append(trace)

    elif orientation == 'h':
        if type(col) is str:
            y = col
            color = 'rgb(0, 0, 0)'

            trace = go.Histogram(
                y=dataFrame[y],
                nbinsy=n_bins,
                marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                name=y[:20],
                orientation=orientation,
                histfunc='count',
                histnorm='percent'
            )

            data.append(trace)

        elif type(col) is list:
            for i in list(range(len(col))):
                y = col[i]
                color = colors[i] if colors is not None else 'rgb(0, 0, 0)'

                trace = go.Histogram(
                    y=dataFrame[y],
                    nbinsy=n_bins,
                    marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                    name=y[:20],
                    orientation=orientation,
                    histfunc='count',
                    histnorm='percent'
                )

                data.append(trace)

    marg = dict(
        pad=5,
        l=w / 5 if orientation == 'h' else 50,
        r=50,
        t=50,
        b=h / 5 if orientation == 'v' else 50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        bargroupgap=barspace,
        bargap=0.2,
        barmode=barmode,
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeBar(dataFrame, vals_cols, cat_col, title='title', y_title='y', x_title='x', orientation='v', barmode='group',
            barspace=0.2, annot=False, size=(900, 500), font='Arial', fontfactor=1, colors=None, legend=True,
            export=False, file_name=''):
    '''

    :param dataFrame: dataframe
    :param vals_cols: columns with the values, list for multiple column names or string for a single one
    :param cat_col: string column name with categories
    :param title: title of the chart, DEFAULT = title
    :param y_title: title of the Y axis, DEFAULT = y
    :param x_title: title of the X axis, DEFAULT = x
    :param orientation: string h for horizontal and v for vertical, DEFAULT = v
    :param barmode: string group for grouped bars and stack for stacked, DEFAULT = group
    :param barspace: percentage (0 to 1) of the width of the bars to be applied as space between group bars, DEFAULT = 0.2
    :param annot: If True adds the total of the bar aligned to the end of the bar, on the top if vertical or left if horizonta, DEFAUL = False
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    annotDict = {str(k)[:20]: round(dataFrame[dataFrame[cat_col] == k][vals_cols].sum(axis=1).values[0], 2) for k in
                 dataFrame[cat_col].tolist()}
    data = []
    w = size[0]
    h = size[1]

    if orientation == 'v':
        annotation = None if annot is False else [
            go.layout.Annotation(
                x=k,
                xanchor='center',
                y=annotDict[k],
                yanchor='bottom',
                xref="x",
                yref="y",
                yshift=0,
                text=annotDict[k],
                showarrow=False,
                ax=0,
                ay=0,
                font=dict(
                    color='rgba(150, 150, 150, 1.0)',
                    size=10 * fontfactor,
                    family=font
                )
            ) for k in annotDict]

        if type(vals_cols) is str:
            y = vals_cols
            x = cat_col
            color = 'rgb(0, 0, 0)'

            trace = go.Bar(
                x=[cat[:20] for cat in dataFrame[x].tolist()],
                y=dataFrame[y],
                marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                name=vals_cols[:20],
                orientation=orientation
            )

            data.append(trace)

        elif type(vals_cols) is list:
            for i in list(range(len(vals_cols))):
                y = vals_cols[i]
                x = cat_col
                color = colors[i] if colors is not None else 'rgb(0, 0, 0)'

                trace = go.Bar(
                    x=[str(cat)[:20] for cat in dataFrame[x].tolist()],
                    y=dataFrame[y],
                    marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                    name=vals_cols[i][:20],
                    orientation=orientation
                )

                data.append(trace)

    elif orientation == 'h':
        annotation = None if annot is False else [
            go.layout.Annotation(
                x=annotDict[k],
                xanchor='left',
                y=k,
                yanchor='middle',
                xref="x",
                yref="y",
                yshift=0,
                text=annotDict[k],
                showarrow=False,
                ax=0,
                ay=0,
                font=dict(
                    color='rgba(150, 150, 150, 1.0)',
                    size=10 * fontfactor,
                    family=font
                )
            ) for k in annotDict]

        if type(vals_cols) is str:
            y = cat_col
            x = vals_cols
            color = 'rgb(0, 0, 0)'

            trace = go.Bar(
                x=dataFrame[x],
                y=[str(cat)[:20] for cat in dataFrame[y].tolist()],
                marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                name=vals_cols[:20],
                orientation=orientation
            )

            data.append(trace)

        elif type(vals_cols) is list:
            for i in list(range(len(vals_cols))):
                y = cat_col
                x = vals_cols[i]
                color = colors[i] if colors is not None else 'rgb(0, 0, 0)'

                trace = go.Bar(
                    x=dataFrame[x],
                    y=[str(cat)[:20] for cat in dataFrame[y].tolist()],
                    marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                    name=vals_cols[i][:20],
                    orientation=orientation
                )

                data.append(trace)

    marg = dict(
        pad=5,
        l=w / 5 if orientation == 'h' else 50,
        r=50,
        t=50,
        b=h / 5 if orientation == 'v' else 50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        annotations=annotation,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        bargroupgap=barspace,
        barmode=barmode,
        bargap=0.2,
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeBarPrint(dataFrame, vals_cols, cat_col, title='title', y_title='y', barmode='group', norm=False, barspace=0.2,
                 x_title='x', annot=False, label_shift = 0, size=(900, 500), font='Arial', fontfactor=1, colors=None, legend=True,
                 export=False, file_name=''):
    '''

    :param dataFrame: dataframe
    :param vals_cols: columns with the values, list for multiple column names or string for a single one
    :param cat_col: string column name with categories
    :param title: title of the chart, DEFAULT = title
    :param y_title: title of the Y axis, DEFAULT = y
    :param x_title: title of the X axis, DEFAULT = x
    :param x_title: if true values are a perfentage of the total for each category, DEFAULT = False
    :param barmode: string group for grouped bars and stack for stacked, DEFAULT = group
    :param barspace: percentage (0 to 1) of the width of the bars to be applied as space between group bars, DEFAULT = 0.2
    :param annot: If True adds the total of the bar aligned to the end of the bar, on the top if vertical or left if horizonta, DEFAUL = False
    :param label_shift: Shifts the label over the bars, begative for down and positive for up, DEFAULT = 0
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    if norm:
        dfList = []

        for cat in dataFrame[cat_col].unique():
            dfTemp = dataFrame[dataFrame[cat_col] == cat].copy()

            for col in vals_cols:
                dfTemp[col + '_p'] = (dfTemp[col] / dfTemp[vals_cols].sum(axis=1)) * 100

            dfList.append(dfTemp[[cat_col] + [x + '_p' for x in vals_cols]])

        dataFrame = pd.concat(dfList)
        dataFrame.columns = [cat_col] + [x for x in vals_cols]

    data = []
    w = size[0]
    h = size[1]

    if type(vals_cols) is str:
        y = cat_col
        x = vals_cols
        color = colors[0] if colors is not None else 'rgb(0, 0, 0)'

        trace = go.Bar(
            x=dataFrame[x],
            y=dataFrame[y],
            marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
            name=vals_cols,
            orientation='h',
            text=[locale.format('%.2f', x, True) if type(x) is float else locale.format('%d', x, True) for x in
                  dataFrame[x].tolist()],
            textposition=None if annot is False else 'inside',
            textfont=dict(
                size=10 * fontfactor,
                family=font
            ),
            textangle=0,
            hoverinfo='name+y+x',
            width=0.5,
            offset=-0.5
        )

        data.append(trace)

    elif type(vals_cols) is list:
        for i in list(range(len(vals_cols))):
            y = cat_col
            x = vals_cols[i]
            color = colors[i] if colors is not None else 'rgb(0, 0, 0)'

            trace = go.Bar(
                x=dataFrame[x],
                y=dataFrame[y],
                marker=dict(color=color, line=dict(width=0, color='rgba(255, 255, 255, 0)')),
                name=vals_cols[i],
                orientation='h',
                text=[locale.format('%.2f', x, True) if type(x) is float else locale.format('%d', x, True) for x in
                      dataFrame[x].tolist()],
                textposition=None if annot is False else 'inside',
                textfont=dict(
                    size=10 * fontfactor,
                    family=font
                ),
                textangle=0,
                hoverinfo='name+y+x',
                width=0.5 if barmode == 'stack' else (1.0 / len(vals_cols))*0.5,
                offset=-0.5 if barmode == 'stack' else -0.29 * (i + 1)
            )

            data.append(trace)

    annotation = []
    
    for cat in dataFrame[cat_col].unique().tolist():
        a = go.layout.Annotation(
            x=0,
            y=cat,
            xanchor='left',
            yanchor='bottom',
            xref="x",
            yref="y",
            yshift = label_shift,
            text=cat,
            showarrow=False,
            ax=0,
            ay=0,
            font=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            )
        )
        
        

        annotation.append(a)

    marg = dict(
        pad=5,
        l=0,
        r=0,
        t=50,
        b=0,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            #             y=0,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showticklabels=False,
            showgrid=False,
            categoryorder='array'
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickformat='d',
            showgrid=False,
            categoryorder='array'
        ),
        annotations=annotation,
        bargroupgap=barspace,
        barmode=barmode,
        bargap=0.1,
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeScatterLine(dataFrame, y_cols, x_col, title='title', x_title='x', y_title='y', marker=True, line=True,
                    boublesize=3, linewidth=1, size=(900, 500), font='Arial', fontfactor=1, colors=None, legend=True,
                    export=False, file_name='', rank=5):
    '''

    :param dataFrame: dataframe
    :param y_cols: columns with the Y axis values, list for multiple column names or string for a single one
    :param x_col: string column name with the X axis values
    :param title: title of the chart, DEFAULT = title
    :param x_title: title of the X axis, DEFAULT = x
    :param y_title: title of the Y axis, DEFAULT = y
    :param marker: boolean to make point markers visible if True or not visible if False, DEFAULT = True
    :param line: boolean to make lines visible if True or not visible if False, DEFAULT = True
    :param boublesize: Integer for constant point markers radius or string for a column name, DEFAUL = 3
    :param linewidth: Integer for line width, DEFAUL = 1
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    data = []
    w = size[0]
    h = size[1]

    # sort dataframe by valeu
    dataFrame = dataFrame.sort_values(by=[x_col], ascending=False)

    marker = 'markers+lines' if (marker and line) else 'lines' if (line is True and marker is False) else 'markers'

    if type(y_cols) is str:
        x = x_col
        y = y_cols
        color = 'rgb(0, 0, 0)'

        trace = go.Scatter(
            x=dataFrame[x],
            y=dataFrame[y],
            mode=marker,
            marker=dict(
                color=color,
                size=boublesize if type(boublesize) is int else dataFrame[boublesize],
                line=dict(
                    width=0,
                    color='rgba(255, 255, 255, 0)'
                )
            ),
            line=dict(
                color=color,
                width=linewidth
            ),
            name=y,
        )

        data.append(trace)

    elif type(y_cols) is list:
        for i in list(range(len(y_cols))):
            x = x_col
            y = y_cols[i]
            show = False if i >= rank else True

            color = colors[i] if i < len(colors) - 1 and colors is not None else 'rgba(0, 0, 0,0.3)'

            trace = go.Scatter(
                showlegend=show,
                x=dataFrame[x],
                y=dataFrame[y],
                mode=marker,
                marker=dict(
                    color=color,
                    size=boublesize if type(boublesize) is int else dataFrame[boublesize],
                    line=dict(
                        width=0,
                        color='rgba(255, 255, 255, 0)'
                    )
                ),
                line=dict(
                    color=color,
                    width=linewidth
                ),
                name=y,
            )

            data.append(trace)

    marg = dict(
        pad=5,
        l=0,
        r=0,
        t=50,
        b=0,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeScatterLine2(dataFrame, y_col, x_col, title='title', x_title='x', y_title='y', names=None, trend=False,
                     marker=True, line=True, boublesize=3, linewidth=1, size=(900, 500), font='Arial', fontfactor=1,
                     colors=None, legend=True, export=False, file_name='', rank=5):
    '''

    :param dataFrame: dataframe or list of datrames containing same data structure (y_col and x_col must be the same)
    :param y_cols: string column name with the Y axis values
    :param x_col: string column name with the X axis values
    :param title: title of the chart, DEFAULT = title
    :param x_title: title of the X axis, DEFAULT = x
    :param y_title: title of the Y axis, DEFAULT = y
    :param names: list of labels to be used in the legend when multiple values are plot, DEFAULT = None
    :param trend: plot a trend line if True, DEFAULT = False
    :param marker: boolean to make point markers visible if True or not visible if False, DEFAULT = True
    :param line: boolean to make lines visible if True or not visible if False, DEFAULT = True
    :param boublesize: Integer for constant point markers radius or string for a column name, DEFAUL = 3
    :param linewidth: Integer for line width, DEFAUL = 1
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    data = []
    w = size[0]
    h = size[1]

    # sort dataframe by valeu
    dataFrame = [x.sort_values(by=[x_col], ascending=False) for x in dataFrame] if type(dataFrame) is list else [
        dataFrame.sort_values(by=[x_col], ascending=False)]

    marker = 'markers+lines' if (marker and line) else 'lines' if (line is True and marker is False) else 'markers'

    for i in list(range(len(dataFrame))):
        df = dataFrame[i]
        name = None if names is None else names[i]
        x = x_col
        y = y_col
        show = False if i >= rank else True
        color = colors[i] if i < len(colors) - 1 and colors is not None else 'rgba(0, 0, 0,0.3)'

        trace = go.Scatter(
            x=df[x],
            y=df[y],
            mode=marker,
            marker=dict(
                color=color,
                size=boublesize if type(boublesize) is int else dataFrame[boublesize],
                line=dict(
                    width=0,
                    color='rgba(255, 255, 255, 0)'
                )
            ),
            line=dict(
                color=color,
                width=linewidth
            ),
            name=name,
        )

        data.append(trace)

        if trend:
            X = df[x_col].values if df[x_col].dtype.kind in ['i', 'f'] else pd.to_datetime(df[x_col]).map(
                dt.datetime.toordinal).values
            y = df[y_col]
            y_fit = np.polyfit(X, y, 1)
            x_fit = np.linspace(X[0], X[-1], X.shape[0])
            y_fit = np.polyval(y_fit, x_fit)

            slope, intercept, r_value, p_value, std_err = stats.linregress(X, y)

            trace = go.Scatter(
                x=df[x_col],
                y=y_fit,
                mode='lines',
                marker=dict(
                    color=color,
                    size=linewidth * 2,
                    line=dict(
                        width=0,
                        color='rgba(255, 255, 255, 0)'
                    )
                ),
                line=dict(
                    color=color,
                    width=linewidth / 2
                ),
                name=name+' trendline (Slope '+str(round(slope, 2))+')',
            )

            data.append(trace)

    marg = dict(
        pad=5,
        l=0,
        r=0,
        t=50,
        b=0,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeScatterLineCat(dataFrame, y_col, x_col, cat_col, timeseries=True, title='title', x_title='x', y_title='y', names=None, trend=False,
                     marker=True, line=True, boublesize=3, linewidth=1, size=(900, 500), font='Arial', fontfactor=1,
                     colors=None, legend=True, export=False, file_name='', rank=5):
    '''

    :param dataFrame: dataframe or list of datrames containing same data structure (y_col and x_col must be the same)
    :param y_col: string column name with the Y axis values
    :param x_col: string column name with the X axis values
    :param cat_col: list or string with category columns, if list categories are concatenated
    :param timeseries: True if x axis is a serie of dates else float, DEFAULT = True
    :param title: title of the chart, DEFAULT = title
    :param x_title: title of the X axis, DEFAULT = x
    :param y_title: title of the Y axis, DEFAULT = y
    :param names: list of labels to be used in the legend when multiple values are plot, DEFAULT = None
    :param trend: plot a trend line if True, DEFAULT = False
    :param marker: boolean to make point markers visible if True or not visible if False, DEFAULT = True
    :param line: boolean to make lines visible if True or not visible if False, DEFAULT = True
    :param boublesize: Integer for constant point markers radius or string for a column name, DEFAUL = 3
    :param linewidth: Integer for line width, DEFAUL = 1
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    data = []
    w = size[0]
    h = size[1]
    
    cat_col = cat_col if type(cat_col) is list else [cat_col]
    
    dfChart = dataFrame.copy()

    for col in cat_col:
        dfChart[col] = dfChart[col].astype(str)    

    dfChart['category'] = dfChart[cat_col].agg(', '.join, axis=1)
    cats = sorted(dfChart['category'].unique().tolist())
    
    dfChart[x_col] = pd.to_datetime(dfChart[x_col], unit='s') if timeseries else dfChart[x_col].astype('float64')
    dfChart[y_col] = dfChart[y_col].astype('float64')

    marker = 'markers+lines' if (marker and line) else 'lines' if (line is True and marker is False) else 'markers'

    for i in list(range(len(cats))):
        dfTrace = dfChart[dfChart['category'] == cats[i]].copy()
        name = cats[i]
        x = x_col
        y = y_col
        show = False if i >= rank else True
        color = 'rgba(0, 0, 0,0.3)' if colors is None else colors if type(colors) is str else colors[i]

        trace = go.Scatter(
            x=dfTrace[x],
            y=dfTrace[y],
            mode=marker,
            marker=dict(
                color=color,
                size=boublesize if type(boublesize) is int else dfTrace[boublesize],
                line=dict(
                    width=0,
                    color='rgba(255, 255, 255, 0)'
                )
            ),
            line=dict(
                color=color,
                width=linewidth
            ),
            name=name,
        )

        data.append(trace)

        if trend:
            X = dfTrace[x_col].values if dfTrace[x_col].dtype.kind in ['i', 'f'] else pd.to_datetime(dfTrace[x_col]).map(dt.datetime.toordinal).values
            y = dfTrace[y_col]
            y_fit = np.polyfit(X, y, 1)
            x_fit = np.linspace(X[0], X[-1], X.shape[0])
            y_fit = np.polyval(y_fit, x_fit)

            slope, intercept, r_value, p_value, std_err = stats.linregress(X, y)

            trace = go.Scatter(
                x=dfTrace[x_col],
                y=y_fit,
                mode='lines',
                marker=dict(
                    color=color,
                    size=linewidth * 2,
                    line=dict(
                        width=0,
                        color='rgba(255, 255, 255, 0)'
                    )
                ),
                line=dict(
                    color=color,
                    width=linewidth / 2
                ),
                name=name+' trendline (Slope '+str(round(slope, 2))+')',
            )

            data.append(trace)

    marg = dict(
        pad=5,
        l=0,
        r=0,
        t=50,
        b=0,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeDonut(dataframe, values, labels, txt=None, treshold=1, treshold_label='other', hole=0.5, title='title',
              size=(500, 500), font='Arial', fontfactor=1, colors=None, legend=True, export=False, file_name=None):
    '''

    :param dataframe: dataframe
    :param values:  string column name with the values
    :param labels:  list column names with categories
    :param txt: dict for central label where the key is the big label and value is the legend below, DEFAULT = None
    :param treshold: percentage where the chart will aggregate everything below, DEFAULT = 1
    :param treshold_label: label for the treshold aggregation, DEFAULT = other
    :param hole: proportion of the hole to the chart, DEFAULT = 0.5
    :param title: title of the chart, DEFAULT = title
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (500, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name:
    :return: filename for the downloaded png, DEFAULT = ''
    '''
    dataframe = dataframe.groupby(labels)[[values]].sum().reset_index()
    h = size[1]
    w = size[0]

    val = dataframe[values].tolist()
    lbl = dataframe[labels].tolist()
    hover = lbl
    lbl = [x+' ('+str(locale.format_string("%.2f", y, True) if type(y) is float else locale.format_string("%d", y, True))+')' for x, y in zip(lbl, val)]
    lbl = [treshold_label if float(y) / sum(val) * 100 < treshold else x for x, y in zip(lbl, val)]

    data = []

    trace = go.Pie(
        labels=lbl,
        values=val,
        marker=dict(colors=colors if type(colors) is list else ['rgb(0, 0, 0)'] * dataframe.shape[0]),
        hole=hole,
        #         hovertext=hover,
        hovertemplate="%{label}<br>%{percent}</br>",
        #         hoverinfo='text+value+percent',
        #         hoverinfo='text+value+percent',
        textfont=dict(
            size=10 * fontfactor,
            family=font
        )
    )

    data.append(trace)

    annotation = None if txt is None else [
        go.layout.Annotation(
            width=size[0] * 0.2,
            font=dict(
                size=20 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            x=0,
            xanchor='center',
            y=0,
            yanchor='bottom',
            xref="x",
            yref="y",
            yshift=0,
            text=[x for x in txt.keys()][0],
            showarrow=False,
            ax=0,
            ay=0
        ),
        go.layout.Annotation(
            width=size[0] * 0.2,
            font=dict(
                size=12 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            x=0,
            xanchor='center',
            y=0,
            yanchor='top',
            xref="x",
            yref="y",
            yshift=0,
            text=[x for x in txt.values()][0],
            showarrow=False,
            ax=0,
            ay=0
        )]

    marg = dict(
        pad=5,
        l=50,
        r=50,
        t=50,
        b=50,
    )

    layout = go.Layout(
        width=size[0],
        height=size[1],
        title=title,
        annotations=annotation,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        xaxis=dict(
            showticklabels=False,
            showgrid=False,
        ),
        yaxis=dict(
            showticklabels=False,
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeSunburst(dataframe, values, levels, labels, level0Label='total', title='title', size=(500, 500), font='Arial',
                 fontfactor=1, colors=None, legend=True, export=False, file_name=''):
    '''

    :param dataframe: dataframe
    :param values: string column name with the values
    :param levels: list column names with categories in order according to their levels hierarchy
    :param labels: list column names with labels in order according to their levels
    :param level0Label: label for the first level, DEFAULT = total
    :param title: title of the chart, DEFAULT = title
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (500, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''
    w = size[0]
    h = size[1]

    levels = levels if type(levels) is list else [levels]

    dfL0 = pd.DataFrame({'parents': '', 'labels': level0Label, 'ids': level0Label, 'values': dataframe[[values]].sum()})
    dfList = [dfL0]

    for i in list(range(len(levels))):
        col = levels[i]
        col_len = 'len_'+col
        col_temp = 'l'+i
        lbl = labels[i]

        dataframe['label'] = dataframe[lbl]
        dataframe[col_len] = dataframe[col].apply(lambda x: len(str(x)))
        dataframe[col] = dataframe[col].apply(
            lambda x: str(x).zfill(dataframe[col_len].max()) if (type(x) is int or type(x) is float) else x)

        if i == 0:
            dataframe[col_temp] = dataframe.apply(lambda row: str(row[col]), axis=1).copy()

            dfTemp = dataframe.groupby([col_temp])[values].sum().reset_index()
            dfTemp['parents'] = level0Label
            dfTemp['label'] = dfTemp[col_temp].apply(lambda x: dataframe[dataframe[col_temp] == x]['label'].tolist()[0])

            dfT = dfTemp[['parents', 'label', col_temp, values]]
            dfT.rename(columns={'parents': 'parents', 'label': 'labels', col_temp: 'ids', values: 'values'},
                       inplace=True)

            #             print(dfT.head(3))

            dfList.append(dfT)

        else:
            dataframe[col_temp] = dataframe.apply(lambda row: str(row[col]) + str(row['l'+str(i - 1)]), axis=1).copy()

            dfTemp = dataframe.groupby(['l'+str({i - 1}), col_temp])[values].sum().reset_index()
            dfTemp['label'] = dfTemp[col_temp].apply(lambda x: dataframe[dataframe[col_temp] == x]['label'].tolist()[0])

            dfT = dfTemp[['l'+str({i - 1}), 'label', col_temp, values]]
            dfT.rename(columns={'l'+str({i - 1}): 'parents', 'label': 'labels', col_temp: 'ids', values: 'values'},
                       inplace=True)

            #             print(dfT.head(3))

            dfList.append(dfT)

    dataframe = pd.concat(dfList, sort=False)
    data = []

    trace = go.Sunburst(
        ids=dataframe['ids'],
        parents=dataframe['parents'],
        labels=dataframe['labels'].apply(lambda x: x[:15]),
        values=dataframe['values'],
        marker=dict(
            colors=colors if type(colors) is list else ['rgb(255, 255, 255)'] + ['rgb(0, 0, 0)'] * dataframe.shape[0],
            line=dict(color='rgb(255, 255, 255)', width=0.5)
        ),
        branchvalues='total',
        textfont=dict(
            size=10 * fontfactor,
            family=font
        )
    )

    data.append(trace)

    marg = dict(
        pad=5,
        l=50,
        r=50,
        t=50,
        b=50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        showlegend=legend,
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeSankey(dataframe, levels, values, orientation='v', title='title', size=(500, 500), font='Arial', fontfactor=1,
               colors=None, export=False, file_name=None):
    '''

    :param dataframe: dataframe
    :param levels: list column names with categories in order according to their levels hierarchy
    :param values: string column name with the values
    :param orientation: string h for horizontal and v for vertical, DEFAULT = v
    :param title: title of the chart, DEFAULT = title
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (500, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''
    w = size[0]
    h = size[1]

    f = h if orientation == 'v' else w

    idList = sorted(['l'+str(i)+'|||'+str(x) for i in list(range(len(levels))) for x in sorted(dataframe[levels[i]].unique().tolist())])
    dfList = []

    for i in list(range(len(levels) - 1)):
        dfTemp = dataframe[[levels[i], levels[i + 1], values]].copy()
        dfTemp = dfTemp.groupby([levels[i], levels[i + 1]]).sum().reset_index()
        dfTemp.columns = ['source', 'target', 'value']

        dfTemp['source'] = dfTemp['source'].apply(lambda x: idList.index('l'+str(i)+'|||'+str(x)))
        dfTemp['target'] = dfTemp['target'].apply(lambda x: idList.index('l'+str(i+1)+'|||'+str(x)))

        dfList.append(dfTemp)

    dataframe = pd.concat(dfList)
    source = dataframe['source'].tolist()
    target = dataframe['target'].tolist()
    value = dataframe['value'].tolist()

    data = []

    trace = go.Sankey(
        node=dict(
            pad=10,
            thickness=int(f / 50),
            line=dict(color='rgb(255, 255, 255)', width=0.5),
            label=[(x.split('|||'))[1] for x in idList],
            color=colors if type(colors) is list else ['rgb(0, 0, 0)'] * dataframe.shape[0],
        ),
        link=dict(
            source=source,
            target=target,
            value=value,
            line=dict(color='rgb(255, 255, 255)', width=0.2),
        ),
        orientation=orientation,
        textfont=dict(
            size=10 * fontfactor,
            family=font
        )
    )

    data.append(trace)

    marg = dict(
        pad=5,
        l=50,
        r=50,
        t=50,
        b=50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeViolin(dataFrame, cat_col, val_col, title='title', y_title='y', x_title='x', orientation='h', side='both',
               box=False, points='outliers', size=(900, 500), font='Arial', fontfactor=1, colors=None, legend=True,
               export=False, file_name=''):
    '''

    :param dataFrame: dataframe
    :param cat_col: string column name with categories
    :param val_col: string column name with values
    :param title: title of the chart, DEFAULT = title
    :param y_title: title of the Y axis, DEFAULT = y
    :param x_title: title of the X axis, DEFAULT = x
    :param orientation: string h for horizontal and v for vertical, DEFAULT = h
    :param side: string to set only positive violin, negative violin or both, DEFAULT = both
    :param box: boolean to make boxplot inside visible or not, DEFAULT =  False
    :param points: string to show outlier points or boolean False to make it inactive, DEFAULT = outliers
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''
    w = size[0]
    h = size[1]

    data = []
    catList = sorted(dataFrame[cat_col].unique().tolist())

    for i in list(range(len(catList))):
        if orientation == 'h':
            trace = go.Violin(
                x=dataFrame[dataFrame[cat_col] == catList[i]][val_col],
                line_color=colors[i] if colors is not None else 'rgb(0, 0, 0)',
                orientation=orientation,
                side=side,
                width=1,
                points=points,
                box=dict(visible=box),
                name=catList[i]
            )

        elif orientation == 'v':
            trace = go.Violin(
                y=dataFrame[dataFrame[cat_col] == catList[i]][val_col],
                line_color=colors[i] if colors is not None else 'rgb(0, 0, 0)',
                orientation=orientation,
                side=side,
                width=3,
                points=points,
                box=dict(visible=box),
                name=catList[i]
            )

        data.append(trace)

    marg = dict(
        pad=5,
        l=50,
        r=50,
        t=50,
        b=50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeHeatmap(dataFrame, title='title', y_title='y', x_title='x', mask=False, size=(900, 500), font='Arial', fontfactor=1,
                colors='viridis', legend=True, export=False, file_name=''):
    '''

    :param dataFrame: dataframe
    :param cat_col: string column name with categories
    :param val_col: string column name with values
    :param title: title of the chart, DEFAULT = title
    :param y_title: title of the Y axis, DEFAULT = y
    :param x_title: title of the X axis, DEFAULT = x
    :param mask: boolean if mask is True, hides the upper diagonal with mirrored values, DEFAULT = False
    :param orientation: string h for horizontal and v for vertical, DEFAULT = h
    :param side: string to set only positive violin, negative violin or both, DEFAULT = both
    :param box: boolean to make boxplot inside visible or not, DEFAULT =  False
    :param points: string to show outlier points or boolean False to make it inactive, DEFAULT = outliers
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: string with existing colorscale (https://plotly.com/python/builtin-colorscales/) or array of positionnal colors like [[0.0, 'rbg(255, 255, 255)'], [0.5, 'rbg(130, 130, 130)'], [1.0, 'rbg(0, 0, 0)']], DEFAULT = Viridis
    :param legend: boolean to make legends visible or not, DEFAULt = True
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''
    w = size[0]
    h = size[1]

    data = []

    corr = dataFrame.iloc[:, :len(dataFrame.columns) - 1].corr()
    
    if mask:
        mask = np.zeros_like(corr, dtype = np.bool)
        mask[np.triu_indices_from(mask)] = True
        mask[np.diag_indices_from(mask)] = False
        corr=corr.mask(mask)
        corr = corr.iloc[::-1]    
        
    trace = go.Heatmap(
        z=corr.values.tolist(),
        zmin=-1.00,
        zmax=1.00,
        colorscale=colors,
        x=corr.columns.tolist(),
        y=corr.index.tolist()
    )

    data = [trace]

    marg = dict(
        pad=5,
        l=150,
        r=0,
        t=150,
        b=50,
    )

    layout = go.Layout(
        width=w,
        height=h,
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        legend=dict(
            x=0,
            xanchor='left',
            y=-0.025,
            yanchor='top',
            traceorder="normal",
            font=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            bgcolor='rgba(0, 0, 0, 0)',
            bordercolor='rgba(0, 0, 0, 0)',
            borderwidth=0
        ),
        yaxis=dict(
            title=y_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        xaxis=dict(
            title=x_title,
            titlefont=dict(
                size=10 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            tickfont=dict(
                size=8 * fontfactor,
                color='rgba(0.0, 0.0, 0.0, 1.0)',
                family=font
            ),
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return


def makeTreeMap(dataframe, labels, values=None, title='title', size=(500, 500), font='Arial', fontfactor=1,
                fonttreshold=10, colors=None, export=False, file_name=None):
    '''

    :param dataFrame: dataframe
    :param labels: string column name with labels
    :param values: string column name with values if not given value for each row is set to 1, DEFAULT = None
    :param title: title of the chart, DEFAULT = title
    :param size: tuple or list with width and height in pixels respectively, DEFAULT = (900, 500)
    :param font: string with font name installed in the system, DEFAULT = Arial
    :param fontfactor: hyde fonts smaller then the given, DEFAULT = 10
    :param fonttreshold: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
    :param colors: list of colors one for each category in values, DEFAULT = None (everything will be black)
    :param export: boolean to download the png or not, DEFAULT = False
    :param file_name: filename for the downloaded png, DEFAULT = ''
    :return:
    '''

    dfTemp = dataframe.copy()
    dfTemp['v'] = dfTemp.apply(lambda x: 1 if values is None else x[values], axis=1)
    dfTemp = dfTemp.groupby([labels])['v'].sum().reset_index()

    h = size[1]
    w = size[0]

    data = []

    trace = go.Treemap(
        labels=dfTemp[labels],
        parents=[''] * dfTemp[labels].shape[0],
        values=dfTemp['v'],
        texttemplate='%{label}<br>%{value} (%{percentParent})</br>',
        textposition='top left',
        textfont=dict(
            size=10 * fontfactor,
            family=font
        ),
        marker=dict(colors=colors if type(colors) is list else ['rgb(0, 0, 0)'] * dfTemp.shape[0]),
    )

    data.append(trace)

    marg = dict(
        pad=5,
        l=50,
        r=50,
        t=50,
        b=50,
    )

    layout = go.Layout(
        width=size[0],
        height=size[1],
        title=title,
        titlefont=dict(
            size=12 * fontfactor,
            color='rgba(0.0, 0.0, 0.0, 1.0)',
            family=font
        ),
        uniformtext=dict(minsize=fonttreshold, mode='hide'),
        xaxis=dict(
            showticklabels=False,
            showgrid=False,
        ),
        yaxis=dict(
            showticklabels=False,
            showgrid=False,
        ),
        paper_bgcolor='rgba(255, 255, 255, 1.0)',
        plot_bgcolor='rgba(255, 255, 255, 1.0)',
        margin=marg,
        separators=',.',
    )

    fig = go.Figure(data=data, layout=layout)

    if export:
        iplot(fig, image='png', filename=file_name, image_width=w, image_height=h)
    else:
        iplot(fig)

    return

# def makeMapPoly(dataFrame, val_col, steps = None, classifier = 'equal', colorMap = 'viridis', size=(500,500), fontfactor=1, export = False, file_name = ''):
#     '''

#     :param dataFrame: dataframe
#     :param val_col: string column name with values
#     :param steps: number of steps for numerical values, leave default for categorical values, DEFAULT = None
#     :param classifier: 'equal' for equal intervals, 'natural' for natural breaks and 'quantiles' for quantiles, DEFAULT = 'equal'
#     :param colorMap: colormap for Matplotlib (https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html), DEFAULT = 'viridis'
#     :param size: tuple or list with width and height in pixels respectively, DEFAULT = (500, 500)
#     :param fontfactor: numeric factor to multiply the fonts for all texts in the chart, DEFAULT = 1
#     :param export: boolean to download the png or not, DEFAULT = False
#     :param file_name: filename for the downloaded png, DEFAULT = ''
#     :return:
#     '''
#     w = size[0]/30
#     h = size[1]/30

#     if steps is None or steps == 0:
#         labs = sorted(dataFrame[val_col].unique().tolist())
#         n = len(labs)
#         cMap = makeColorMap(mplCMap = colorMap, steps = n)

#     else:
#         n = steps
#         cMap = makeColorMap(mplCMap = colorMap, steps = n)

#         if classifier == 'equal':
#             labs = ps.viz.mapclassify.EqualInterval(dataFrame[val_col].tolist(), n)
#             labs = labs.bins

#         elif classifier == 'natural':
#             labs = ps.viz.mapclassify.NaturalBreaks(dataFrame[val_col].tolist(), n)
#             labs = labs.bins

#         elif classifier == 'quantiles':
#             labs = ps.viz.mapclassify.Quantiles(dataFrame[val_col].tolist(), n)
#             labs = labs.bins

#     fig, ax = plt.subplots(1, figsize=(w, h))
#     handles = []
#     colors = {}

#     for i in list(range(n)):
#         if steps is None or steps == 0:
#             fltr = dataFrame[val_col] == labs[i]
#             dataFrame[fltr].plot(linewidth = 0.1, ax = ax, color = cMap[i], edgecolor = 'black')

#         else:
#             if i == 0:
#                 fltr = dataFrame[val_col] <= labs[i]
#                 dataFrame[fltr].plot(linewidth = 0.1, ax = ax, color = cMap[i], edgecolor = 'black')

#             else:
#                 f1 = dataFrame[val_col] <= labs[i]
#                 f2 = dataFrame[val_col] > labs[i-1]
#                 dataFrame[f1 & f2].plot(linewidth = 0.1, ax = ax, color = cMap[i], edgecolor = 'black')


#         p = mpatches.Patch(color = cMap[i], label = labs[i])
#         handles.append(p)

#     ax.set_axis_off()
#     ax.legend(handles = handles)

#     if export:
#         plt.savefig(file_name+'.png')

#     return


def printTest():
    print('OK')

    return


########################################################################################################################


########################################################################################################################

if __name__ == "__main__":
    print()
