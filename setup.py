from setuptools import setup

setup(
    name='tml_dash',
    version='2.0.3',
    description='Charts functions based on Plotly',
    url='git@gitlab.com:dedsresende/tm_dash.git',
    author='Andre Resende',
    author_email='dedsresende@gmail.com',
    license='unlicense',
    packages=['tml_dash'],
    zip_safe=False,
    install_requires=[
       'pandas >= 0.24.2',
       'Shapely >= 1.6.4.post2',
       'numpy >= 1.13.3',
       'colormap >= 1.0.3',
       'matplotlib >= 3.2.2',
       'plotly >= 4.8.2',
       'scipy >= 1.5.1'
    ]
)
